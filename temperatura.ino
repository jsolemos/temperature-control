#include "SevSeg.h" // https://github.com/DeanIsMe/SevSeg.git 
//#include <Adafruit_Sensor.h> // https://github.com/adafruit/Adafruit_Sensor.git
#include <DHT.h> // https://github.com/adafruit/DHT-sensor-library.git
#include <DHT_U.h> 

SevSeg sevseg; // instancia do objeto de controle de exibição de informação com os led de segmento

#define MAX_NUMBER_STRINGS 4
#define MAX_STRING_SIZE 8
char infors[MAX_NUMBER_STRINGS][MAX_STRING_SIZE];

#define PATTERN_CHANGE_TIME 5000
#define PATTERN_CHANGE_TIME_SEPARATOR 500
unsigned long timer = millis() - PATTERN_CHANGE_TIME_SEPARATOR;
byte pos = 0;
int count = 0;

#define DHTPIN A2 // pino que o sinal do sensor está conectado
#define DHTTYPE DHT11 // DHT 11

#define MAX_COUNT 100 // para calcular a média de temperatura e umidade

#define RELEPIN A0 // pino que o sinal do relé está conectado

DHT dht(DHTPIN, DHTTYPE); // Instancia do controlador 

float sumH = 0.0;
float sumT = 0.0;

void setup() {
  Serial.begin(115200); //INICIALIZA A SERIAL
  byte numDigits = 4;
  byte digitPins[] = {2, 3, 4, 5};
  byte segmentPins[] = {6, 7, 8, 9, 10, 11, 12, 13};
  bool resistorsOnSegments = false; // 'false' means resistors are on digit pins
  byte hardwareConfig = COMMON_ANODE; // See README.md for options
  bool updateWithDelays = false; // Default. Recommended
  bool leadingZeros = false; // Use 'true' if you'd like to keep the leading zeros
 
  
  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments, updateWithDelays, leadingZeros);
  sevseg.setBrightness(10);

  // Adds set of test strings with periods in various places
  strcpy(infors[0], "00.0C");
  strcpy(infors[1], "-.-.-.-");
  strcpy(infors[2], "00.0H");
  strcpy(infors[3], "-.-.-.-");

  dht.begin();
  delay(2000);
  pinMode(RELEPIN, OUTPUT);
}

void loop() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  
  sumH+=h;
  sumT+=t;
  
  count++;

  // substituímos as informações para exibir no display
  sprintf(infors[0],"%sC", String(t,1).c_str());
  sprintf(infors[2],"%sU", String(h, 1).c_str());
  
  //
  int delaynum = pos%2 == 0 ?  PATTERN_CHANGE_TIME_SEPARATOR : PATTERN_CHANGE_TIME;
  
  // Lógica para alternar a exibição das informações de temperatura e umidade
  if (millis() > (timer + delaynum)) {
    sevseg.setChars(infors[pos]);
    pos++;
    if (pos >= MAX_NUMBER_STRINGS){
      pos = 0;
    }
    timer = millis();
  }
  
  Serial.println((sumT/count));
  // LIGA RELÉ SE A MÉDIA DA TEMPERATURA CAPTURADA FOR 28ºC+
  if((sumT/count) >= 28.0){
    analogWrite(RELEPIN, 255);  
  }else{
    analogWrite(RELEPIN, 0);  
  }
  
  if(count >= MAX_COUNT){
    count = 0;
    sumT=0.0;
    sumH=0.0;
  }
  
 sevseg.refreshDisplay(); // Must run repeatedly  
}
